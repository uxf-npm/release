const GitLab = require("./src/GitLab");
const Slack = require("./src/Slack");

module.exports = async (dryRun = false) => {
  const lastTag = await GitLab.getLastTag();

  const commits = await GitLab.loadCommits(
    lastTag ? lastTag.commit.committed_date : null
  );

  await GitLab.createRelease(commits, dryRun);

  await Slack.notify(lastTag, commits, dryRun);
};
