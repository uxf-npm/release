# @uxf/release
[![npm](https://img.shields.io/npm/v/@uxf/release)](https://www.npmjs.com/package/@uxf/release)
[![size](https://img.shields.io/bundlephobia/min/@uxf/release)](https://www.npmjs.com/package/@uxf/release)
[![quality](https://img.shields.io/npms-io/quality-score/@uxf/release)](https://www.npmjs.com/package/@uxf/release)
[![license](https://img.shields.io/npm/l/@uxf/release)](https://www.npmjs.com/package/@uxf/release)

!!! DEPRECATED !!!

Use package [@uxf/scripts](https://www.npmjs.com/package/@uxf/scripts)