#!/usr/bin/env node

require("../cli")()
  .then((exitCode) => {
    process.exitCode = exitCode;
  })
  .catch(() => {
    process.exitCode = 1;
  });
