const { env } = require("process");
const { create } = require("axios");
const moment = require("moment");

const axios = create({
  baseURL: `${env.CI_SERVER_URL}/api/v4`,

  headers: {
    Authorization: `Bearer ${env.GITLAB_TOKEN}`,
  },
});

function generateCommitMessage(commit) {
  const {
    title,
    titleType,
    titleScope,
    short_id,
    web_url,
    author_email,
  } = commit;

  return titleType
    ? `- **${titleScope}** ${title} [${short_id}](${web_url}) (${author_email})`
    : `- ${title} [${short_id}](${web_url}) (${author_email})`;
}

function generateMessage(commits) {
  const features = [];
  const fixes = [];
  const others = [];

  commits.forEach((c) => {
    switch (c.titleType) {
      case "fix":
        fixes.push(generateCommitMessage(c));
        break;
      case "feat":
        features.push(generateCommitMessage(c));
        break;
      default:
        others.push(generateCommitMessage(c));
        break;
    }
  });

  let message = "";
  if (features.length > 0) {
    message += `### New features\n${features.join("\n")}\n`;
  }
  if (fixes.length > 0) {
    message += `### Bug fixes\n${fixes.join("\n")}\n`;
  }
  if (others.length > 0) {
    message += `### Other commits\n${others.join("\n")}\n`;
  }

  return message;
}

function prepareCommits(commits) {
  return commits.map((commit) => {
    const parsed = commit.title.match(/^(fix|feat)\((.*)\):(.*)/) || [];

    const [_, type, scope, message] = parsed;

    return {
      ...commit,
      title: message ? message.trim() : commit.title,
      titleType: type,
      titleScope: scope,
    };
  });
}

async function loadCommits(from) {
  const commits = [];
  console.log(`- start date: ${from}`);

  let nextPage = "1";
  do {
    const response = await axios.get(
      `/projects/${env.CI_PROJECT_ID}/repository/commits`,
      {
        params: {
          ref_name: "master",
          since: from ? moment(from).format() : undefined,
          per_page: "100",
          page: nextPage,
        },
      }
    );
    nextPage = response.headers["x-next-page"];
    commits.push(...response.data);
  } while (nextPage);

  return prepareCommits(commits.filter((c) => c.created_at !== from));
}

async function getLastTag() {
  const response = await axios.get(
    `/projects/${env.CI_PROJECT_ID}/repository/tags`,
    {
      params: {
        search: "^release-",
      },
    }
  );

  const tags = response.data;

  if (tags.length === 0) {
    console.log("- release tag not found");
    return null;
  }

  console.log(`- last tag: ${tags[0].name}`);
  return tags[0];
}

async function createRelease(commits, dryRun = false) {
  const description = generateMessage(commits);
  const tag = "release-" + moment().format("YYYY-MM-DD-HH-mm");

  if (dryRun) {
    console.log(
      `\n🎉🎉🎉 Release "${tag}" published (skipped in dry run)\n\n${description}`
    );
    return;
  }

  await axios.post(
    `/projects/${env.CI_PROJECT_ID}/releases`,
    { description, tag_name: tag, ref: 'master' }
  );

  console.log(`\n🎉🎉🎉 Release "${tag}" published\n\n${description}`);
}

module.exports = {
  loadCommits,
  getLastTag,
  createRelease,
};
