const { env } = require("process");
const { create } = require("axios");
const Logger = require("./Logger");

const axios = create();

function generateCommitMessage(commit) {
  const {
    titleType,
    titleScope,
    title,
    web_url,
    short_id,
    author_email,
  } = commit;
  return titleType
    ? `• *${titleScope}* ${title} <${web_url}|${short_id}> (${author_email})`
    : `• ${title} <${web_url}|${short_id}> (${author_email})`;
}

function generateMessage(commits) {
  const features = [];
  const fixes = [];
  const others = [];

  commits.forEach((c) => {
    switch (c.titleType) {
      case "fix":
        fixes.push(generateCommitMessage(c));
        break;
      case "feat":
        features.push(generateCommitMessage(c));
        break;
      default:
        others.push(generateCommitMessage(c));
        break;
    }
  });

  let message =
    ":tada::tada::tada: Právě bylo nasazeno na produkci :beer::clinking_glasses:";
  if (features.length > 0) {
    message += `\n\n*Nové funkcionality*\n${features.join("\n")}`;
  }
  if (fixes.length > 0) {
    message += `\n\n*Opravy bugů*\n${fixes.join("\n")}`;
  }
  if (others.length > 0) {
    message += `\n\n*Ostatní commity*\n${others.join("\n")}`;
  }

  return message;
}

async function notify(tag, commits, dryRun) {
  if (env.SLACK_WEBHOOK_URL) {
    const text = generateMessage(commits);

    if (dryRun) {
      Logger.info(`Slack nottification (skipped in dry run)\n\n${text}`);
      return;
    }

    await axios.post(env.SLACK_WEBHOOK_URL, { text });
  }
}

module.exports = {
  notify,
};
