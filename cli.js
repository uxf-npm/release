const { argv, env } = require("process");

module.exports = async () => {
  const cli = require("yargs")
    .command("$0", "UXF release helper", (yargs) => {
      yargs.demandCommand(0, 0).usage(`UXF release helper
Usage:
  uxf-release [options]
  
Environment variables:
  GITLAB_TOKEN      - required
  CI_SERVER_URL     - required - setting by GitLab CI
  CI_PROJECT_ID     - required - setting by GitLab CI
  SLACK_WEBHOOK_URL - optional`);
    })
    .option("p", {
      alias: "project-id",
      describe: "GitLab project id (or use environment variable CI_PROJECT_ID)",
      type: "number",
      group: "Options",
    })
    .option("d", {
      alias: "dry-run",
      describe: "Skip publishing and sending slack notification",
      type: "boolean",
      group: "Options",
    })
    .option("h", { alias: "help", group: "Options" })
    .strict(false)
    .exitProcess(false);

  try {
    const { help, p: projectId, d: dryRun, ...options } = cli.parse(
      argv.slice(2)
    );

    if (Boolean(help)) {
      return 0;
    }

    if (projectId) {
      env.CI_PROJECT_ID = projectId;
    }

    if (!env.CI_SERVER_URL) {
      console.log(
        "GitLab url must be set. Use environment variable CI_SERVER_URL."
      );
      return 1;
    }

    if (!env.CI_PROJECT_ID) {
      console.log(
        "Project id must be set. Use --project-id option or environment variable CI_PROJECT_ID."
      );
      return 1;
    }

    if (!env.GITLAB_TOKEN) {
      console.log("Environment variable GITLAB_TOKEN is empty.");
      return 1;
    }

    await require("./index")(dryRun);
  } catch (e) {
    console.error(e);
    return 1;
  }
};
